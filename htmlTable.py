import re

class Cell:
    def __init__(self):
        self.Text = ""
        self.Merged = False
        self.BackColor = "#FFFFFF"
        self.FrontColor = "#000000"

class HtmlTable:
    def __init__(self):
        self.Border = 0
        self.CellPadding = 3
        self.CellSpacing = 2
        self.Cells = []
        self.MaxCols = 0
        self.BorderColor = "#FFFFFF"
        self.colWidths = []

    # Create a cell and pad table with empty cells if it does not exist
    # This method must be called at the beginning of every function that modifies cells
    def _checkCellExists(self, row, column):
        # Set the new number of columns in the table
        if self.MaxCols < column + 1:
            self.MaxCols = column + 1

        # Create rows that don't exist and create empty columns in those
        if len(self.Cells) < row + 1:
            oldLen = len(self.Cells)
            for i in range(row - len(self.Cells) + 1):
                self.Cells.append([])
                for _ in range(self.MaxCols):
                    self.Cells[oldLen + i].append(Cell())
        
        # Create columns in existing rows that have less columns than the new column value
        for i in range(len(self.Cells)):
            if len(self.Cells[i]) < self.MaxCols:
                for _ in range(self.MaxCols - len(self.Cells[i])):
                    self.Cells[i].append(Cell())

        # Update list of column widths if column didn't previously exist in table
        if self.MaxCols + 1 > len(self.colWidths):
            for i in range(self.MaxCols - len(self.colWidths) + 1):
                self.colWidths.append(0)

    # Set text in a cell
    def setCellText(self, row, column, text):
        self._checkCellExists(row, column)

        self.Cells[row][column].Text = text

    # Append text to cell
    def appendCellText(self, row, column, text):
        self._checkCellExists(row, column)

        self.Cells[row][column].Text += text

    # Methods for setting the background and text color of cell
    def setCellBackColor(self, row, column, color):
        self._checkCellExists(row, column)

        self.Cells[row][column].BackColor = color
    
    def setCellTextColor(self, row, column, color):
        self._checkCellExists(row, column)

        self.Cells[row][column].FrontColor = color

    # Set hyperlink of cell
    def setCellTextHyperlink(self, row, column, link, text):
        self._checkCellExists(row, column)

        self.Cells[row][column].Text = "<a href=\"" + link + "\">" + text + "</a>"

    def appendCellTextHyperlink(self, row, column, link, text):
        self._checkCellExists(row, column)

        self.Cells[row][column].Text += "<a href=\"" + link + "\">" + text + "</a>"

    def setColumnWidth(self, column, value):
        self.colWidths[column] = value

    def _getTextContents(self, row, column):
        cellText = self.Cells[row][column].Text
        result = re.sub("<a href=[^>]+>", "", cellText)
        result = re.sub("</a>", "", result)
        return result

    def setOptimalColumnWidth(self, column = -1):
        if column == -1:
            for i in range(len(self.Cells)):
                for j in range(len(self.Cells[i])):
                    # TODO: This is a rough approximation for a small font. Make column width dynamic based on font/glyph width
                    if self.Cells[i][j].Merged == True:
                        continue
                    # Strip cell's text of hyperlinks
                    textContents = self._getTextContents(i, j)
                    maxLineLengthInText = len(max(textContents.split('\n'), key=len))
                    currentWidth = maxLineLengthInText * 6
                    if self.colWidths[j] < currentWidth:
                        self.colWidths[j] = currentWidth
        else:
            for i in range(len(self.Cells)):
                if self.Cells[i][column].Merged == True:
                    continue
                # Strip cell's text of hyperlinks
                textContents = self._getTextContents(i, column)
                maxLineLengthInText = len(max(textContents.split('\n'), key=len))
                currentWidth = maxLineLengthInText * 6
                if self.colWidths[column] < currentWidth:
                    self.colWidths[column] = currentWidth

    # Replaces all columns with a single column that has the contents of the first column in the original row
    # Sets the colspan to the number of columns currently in the table
    def mergeRowCells(self, row):
        self.Cells[row] = self.Cells[row][:1]
        self.Cells[row][0].Merged = True

    def setPadding(value):
        self.CellPadding = value

    def setBorderWidth(value):
        self.Border = value

    def setCellSpacing(value):
        self.CellSpacing = value

    def write(self, outFile, generateTableOnly: bool):
        f = open(outFile, "w", encoding="utf-8")

        # Begin HTML
        if generateTableOnly:
            f.write("<table")
        else:
            f.write("<html>\n<body>\n\n<table")

        # Set table formatting
        f.write(" border=" + str(self.Border))
        f.write(" cellspacing=" + str(self.CellSpacing))
        f.write(" bordercolor=" + self.BorderColor)

        # End <table> properties
        f.write(">\n")

        for colWidth in self.colWidths:
            f.write("\t<colgroup width=\"" + str(colWidth) + "\"></colgroup>\n")

        # Write all cells to table
        for row in self.Cells:
            # Begin row
            f.write("\t<tr>\n")

            for cell in row:
                # Begin cell
                f.write("\t\t<td")
                
                # Set cell formatting
                if cell.Merged == True:
                    f.write(" colspan=" + str(self.MaxCols))
                f.write(" bgcolor=" + cell.BackColor)
                f.write(" style=\"color: " + cell.FrontColor + "; padding: " + str(self.CellPadding) + "px\"")
                f.write(">")
                
                # Replace C special characters with HTML equivalents
                cell.Text = cell.Text.replace("\n", "<br>")
                cell.Text = cell.Text.replace("\t", "&emsp;")

                # Write text
                f.write(cell.Text)

                # End cell
                f.write("</td>\n")

            # End row
            f.write("\t</tr>\n")

        # End HTML
        if generateTableOnly:
            f.write("</table>")
        else:
            f.write("</table>\n\n</body>\n</html>")

        f.close()
